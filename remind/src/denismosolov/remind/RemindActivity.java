package denismosolov.remind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class RemindActivity extends Activity {
		
	private String _value;
			
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.remember_loop);        
        setValue(Generator.generateRandom());
          
        final TextView number = (TextView) findViewById(R.id.textValue);
        number.setText(getValue());
    }

    public void btn_next_Click(View v)
    {
    	Intent storage = new Intent(getApplicationContext(), RememberLoopActivity.class);
    	storage.putExtra("control", _value);
        startActivity(storage);	
    }

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		this._value = value;
	}    
}