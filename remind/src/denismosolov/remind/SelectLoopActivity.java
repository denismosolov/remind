package denismosolov.remind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SelectLoopActivity extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.select);
    }
	
    public void btn_next_Click(View v)
    {
    	final EditText answer = (EditText) findViewById(R.id.editText1);
    	Intent storage;
    	if (getIntent().getStringExtra("control").equals(answer.getText().toString()))
    	{
        	storage = new Intent(getApplicationContext(), YesAcrivity.class);
    	} else {
        	storage = new Intent(getApplicationContext(), NoAcrivity.class);
    	}
    	storage.putExtra("control", getIntent().getStringExtra("keep"));
    	startActivity(storage);
    }	
}
