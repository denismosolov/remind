package denismosolov.remind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class YesAcrivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.yes);
    }
    
    public void btn_next_Click(View v)
    {
    	Intent storage = new Intent(getApplicationContext(), RememberLoopActivity.class);
    	//storage.putExtra("keep", getIntent().getStringExtra("keep"));
    	storage.putExtra("control", getIntent().getStringExtra("control"));
        startActivity(storage);	
    }
    
}
